#include <OneWire.h>
#include <DallasTemperature.h>

const int oneWirePin = 8;
OneWire oneWire(oneWirePin);
DallasTemperature sensors(&oneWire);

void setup(void)
{
    Serial.begin(9600);

    sensors.begin();
}

void loop(void)
{ 
    sensors.requestTemperatures();
    // Serial.print("Stuff...");
    Serial.println(sensors.getTempCByIndex(0));  
    delay(10);
}
