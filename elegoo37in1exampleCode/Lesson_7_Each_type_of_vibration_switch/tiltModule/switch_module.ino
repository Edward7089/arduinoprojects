const int ledPin = 13;
const int buttonPin = 8;
int sensorVal;

void setup() {
    pinMode(ledPin, OUTPUT);
    pinMode(buttonPin, INPUT);
    Serial.begin(9600);
}
void loop() {
    // val = digitalRead(buttonPin);
    // if (val == HIGH) {
    //     digitalWrite(ledPin, HIGH);
    // } else {
    //     digitalWrite(ledPin, LOW);
    // }
    sensorVal = digitalRead(buttonPin);
    digitalWrite(ledPin, sensorVal);
    if (sensorVal == HIGH) {
        Serial.println("Activity Detected!");
    }
}
