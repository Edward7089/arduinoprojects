
const int buzzerPin = 8;
const int tempo = 520;

class note {
    private:
        char names[9] = {' ', 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C'};
        int freqs[9] = {0, 262, 294, 330, 349, 392, 440, 494, 523};

    public:
        char letter;
        float frequency, len;

        note(char inNote, float inLen): letter(inNote), len(inLen * tempo) {
            for (unsigned int i = 0; i < sizeof(freqs); i++) {
                if (names[i] == letter) {
                    frequency = freqs[i];
                }
            }
        }
        note(): frequency(0), len(1) {}

        void play() {
            tone(buzzerPin, frequency, len);
            delay(len);
        }
};

const char letters[] = {"cdaf gf adfgc gc adcgfe "};
const int beats[] = {1, 1, 1, 1, 1, 4, 4, 2, 1, 1, 1, 1, 1, 1, 4, 4, 3, 1, 2, 1, 2, 1, 2, 2, 3};
const int totalNotes = sizeof(letters);
note sequence[totalNotes];

void getSequence(const char letterSeq[], const int beatSeq[]) {
    for (int i = 0; i < totalNotes; i++) {
        sequence[i] = note(letters[i], beats[i] / 4.0);
    }
}


void setup() {
    Serial.begin(9600);
    pinMode(buzzerPin, OUTPUT);
    getSequence(letters, beats);
}

void loop() {
    for (int i = 0; i < totalNotes; i++) {sequence[i].play();}
    Serial.println("Looping...");
}
