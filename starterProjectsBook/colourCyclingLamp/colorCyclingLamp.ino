const int pins[3] = {10, 9, 11};

const int pause = 10;
const int offset[3] = {0, 85, 171};
int values[3] = {0, 0, 0};
int cycle = 0;

void writeLED(int values[3]) {
    for (int i = 0; i < 3; i++) {
        if (values[i] < 0 || values[i] > 255) {
            continue;
        }
        analogWrite(pins[i], values[i]);
    }
}

void setup() {
    for (int i = 0; i < 3; i++) {
        pinMode(i, OUTPUT);
    }
}

void loop() {
    int squared = cycle * cycle;
    values[0] = 255 - squared - (cycle * 85);
    values[1] = 14790 - squared - (cycle * 256);
    values[2] = 43860 - squared - (cycle * 426);
    writeLED(values);
    cycle = (cycle + 1) % 256;
    delay(pause);
}
